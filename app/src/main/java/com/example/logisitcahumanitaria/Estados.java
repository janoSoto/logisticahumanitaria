package com.example.logisitcahumanitaria;

import java.io.Serializable;

public class Estados implements Serializable {
    private int id;
    private String nombre;
    private int status;

    public Estados(){
        setId(0);
        setNombre("");
        setStatus(0);
    }

    public Estados(int id, String nombre, int status){

        this.id = id;
        this.nombre = nombre;
        this.status = status;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
