package com.example.logisitcahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

public class ListaActivity extends ListActivity
{
    DBEstados dbEstados;
    Estados estado;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        Button btnNuevo = (Button)findViewById(R.id.btnNuevo);
        dbEstados = new DBEstados(this);
        estado = new Estados();
        ArrayList<Estados> Estados = dbEstados.obtener_();
        if ((Estados.isEmpty()))
        {
            Log.d(TAG, "onCreate: VACIO");
            for (int i = 0; i < 10; i++)
            {
                estado.setId(i+1);
                estado.setNombre("Mazatlan" + i+1);
                estado.setStatus(0);
                dbEstados.insert(estado);
            }
        }
        MyArrayAdapter adapter = new MyArrayAdapter(this, R.layout.layout_estados, Estados);
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
        }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estados>
    {
        Context context;
        int textViewResourceId;
        ArrayList<Estados> objects;
        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Estados> objects)
        {
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup)
        {
            LayoutInflater layoutInflater =(LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre =(TextView)view.findViewById(R.id.lblNombreEstado);
            TextView lblID =(TextView)view.findViewById(R.id.lblID);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getNombre());
            lblID.setText(""+objects.get(position).getId());
            btnBorrar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Log.e("",""+objects.get(position).getId());
                    dbEstados.delete(objects.get(position).getId());
                    objects.remove(position);
                    notifyDataSetChanged();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(oBundle);
                    setResult(Activity.RESULT_OK, i);
                    finish();
                }
            });
            return view;
        }
    }
}