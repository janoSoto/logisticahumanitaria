package com.example.logisitcahumanitaria;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final ArrayList<Estados> estados = new ArrayList<Estados>();
    EditText txtSerie;
    Button btnAgregar;
    Button btnLimpiar;
    Button btnListar;
    Estados saveEstado;
    int savedIndex;

    public void limpiar(){
        saveEstado = null;
        txtSerie.setText("");
    }

    protected void onActivityResult( int requestCode, int resultCode, Intent intent){
        super.onActivityResult(requestCode,resultCode,intent);
        if(intent != null){

            Bundle oBundle  = intent.getExtras();
            saveEstado = (Estados)oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            txtSerie.setText(saveEstado.getNombre());
        }
        else{
            limpiar();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtSerie = (EditText)findViewById(R.id.txtSerie);
        btnAgregar = (Button) findViewById(R.id.agregar);
        btnLimpiar =(Button)findViewById(R.id.limpiar);
        btnListar = (Button)findViewById(R.id.listar);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtSerie.getText().toString().equals(""))
                {
                    Toast.makeText(MainActivity.this, R.string.mensajeerror, Toast.LENGTH_SHORT).show();
                }
                else
                {
                    Estados nEstado = new Estados();
                    DBEstados dbEstados=new DBEstados(MainActivity.this);
                    int index= estados.size();
                    if(saveEstado != null)
                    {
                        nEstado.setId(saveEstado.getId());
                        nEstado.setNombre(txtSerie.getText().toString());
                        dbEstados.update(nEstado);
                        Toast.makeText(MainActivity.this,"Modificado", Toast.LENGTH_SHORT).show();

                        limpiar();
                    }else{
                        nEstado.setNombre(txtSerie.getText().toString());
                        dbEstados.insert(nEstado);
                        Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                        saveEstado = null;
                        limpiar();
                    }

                }
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                Bundle bObject= new Bundle();
                bObject.putSerializable("contactos",estados);
                i.putExtras(bObject);
                startActivityForResult(i,0);
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

    }
}
