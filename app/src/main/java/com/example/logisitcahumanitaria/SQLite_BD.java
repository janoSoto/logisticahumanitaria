package com.example.logisitcahumanitaria;

public class SQLite_BD {
    public static final String DATABASE_NAME = "LogisticaHumanitaria";
    //Version de la Base de Datos (Este parámetro es importante  )
    public static final int DATABASE_VERSION = 1;

    //Una clase estatica en la que se definen las propiedaes que determinan la tabla Notes
    // y sus 4 columnas
    public static class Estado {
        //Nombre de la tabla
        public static final String TABLE_NAME = "Estado";
        //Nombre de las Columnas que contiene la tabla
        public static final String ID = "id";
        public static final String ID_Nombre = "nombre";
        public static final String STATUS = "login_status";
    }

    //Setencia SQL que permite crear la tabla Notes
    public static final String TABLE_CREATE =
            "CREATE TABLE " + Estado.TABLE_NAME + "( " +
                    Estado.ID +" INTEGER PRIMARY KEY AUTOINCREMENT ,"+
                    Estado.ID_Nombre +" TEXT ,"+
                    Estado.STATUS + " INTEGER );";

    //Setencia SQL que permite eliminar la tabla Notes
    public static final String Estado_TABLE_DROP = "DROP TABLE IF EXISTS " + Estado.TABLE_NAME;


}
